/**
 * Solution to remove duplicates from array
 * Link : https://leetcode.com/problems/remove-duplicates-from-sorted-array/
 */
public class RemoveDuplicatesFromArray {
    public int removeDuplicates(int[] nums) {
        int len = 0;
        int size = nums.length;
        if (size == 0 || size == 1) {
            return size;
        }
        for (int i = 0; i < size - 1; i++) {
            if (nums[i] != nums[i+1]) {
                nums[len++] = nums[i];
            }
        }
        nums[len++] = nums[size-1];
        return len;
    }

    public static void main(String[] args) {
        RemoveDuplicatesFromArray removeDuplicates = new RemoveDuplicatesFromArray();
        int[] nums = new int[]{0,0,1,1,1,2,2,3,3,4};
        int len = removeDuplicates.removeDuplicates(nums);
        System.out.println(len);
        for (int i = 0; i < nums.length; i++) {
            System.out.println(nums[i]);
        }

    }
}
