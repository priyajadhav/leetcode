/**
 * Solution for Symmetric Tree
 * Link : https://leetcode.com/problems/symmetric-tree/
 */
class SymmetricTree {
    private boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null && q == null)
            return true;
        if (p != null && q != null) {
            if (p.val != q.val)
                return false;
            return isSameTree(p.left, q.right) && isSameTree(p.right, q.left);
        }
        return false;
    }

    public boolean isSymmetric(TreeNode root) {
        if (root == null)
            return true;
        return isSameTree(root.left, root.right);
    }

    public static void main(String[] args) {
        TreeNode t1 = new TreeNode(1);
        t1.left = new TreeNode(3);
        t1.right = new TreeNode(2);
        t1.left.left = new TreeNode(5);

        System.out.println(new SymmetricTree().isSymmetric(t1));
    }
}
