/**
 * Solution to check Palindrome Number
 * Link : https://leetcode.com/problems/palindrome-number/
 */
public class PalindromeNumber {
    public boolean isPalindrome(int x) {
        if (x < 0) return false;
        if (x < 10) return true;
        int originalNumber = x, reversedNumber = 0;
        while(x != 0) {
            int lastDigit = x % 10;
            x = x / 10;
            reversedNumber = reversedNumber * 10 + lastDigit;
        }
        return originalNumber == reversedNumber;
    }

    public static void main(String[] args) {
        PalindromeNumber reverseInteger = new PalindromeNumber();
        System.out.println(reverseInteger.isPalindrome(12321));
    }
}
