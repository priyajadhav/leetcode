import java.util.Stack;

/**
 * Solution to check valid parenthesis
 * Link : https://leetcode.com/problems/valid-parentheses/
 */
public class ValidParentheses {
    public boolean isValid(String s) {
        if (s == null || s.equals(""))
            return true;
        char[] chars = s.toCharArray();
        Stack stack = new Stack<>();
        for (char ch : chars) {
            if ('(' == ch || '[' == ch || '{' == (ch)) {
                stack.push(ch);
            } else {
                if (stack.isEmpty()) return false;
                if (')' == ch && !stack.pop().equals('(')) return false;
                else if (']' == ch && !stack.pop().equals('[')) return false;
                else if ('}' == ch && !stack.pop().equals('{')) return false;
            }
        }
        return stack.isEmpty();
    }

    public static void main(String[] args) {
        ValidParentheses validParentheses = new ValidParentheses();
        System.out.println(validParentheses.isValid("()[{}]"));

    }
}
