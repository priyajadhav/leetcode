/**
 * Solution for Best Time to Buy and Sell Stock
 * Link : https://leetcode.com/problems/best-time-to-buy-and-sell-stock/
 */
public class Stock {
    public int maxProfit(int[] prices) {
        int maxProfit = 0;
        if (prices == null || prices.length == 0) {
            return maxProfit;
        }
        for (int i = 0; i < prices.length; i++) {
            for (int j = i+1; j < prices.length; j++) {
                int profit = prices[j] - prices[i];
                maxProfit = profit > maxProfit ? profit : maxProfit;
            }
        }
        return maxProfit;
    }

    public int maxProfit1(int[] prices) {
        int maxProfit = 0;
        if (prices == null || prices.length == 0) {
            return maxProfit;
        }
        int minPrice = Integer.MAX_VALUE;
        for (int i = 0; i < prices.length; i++) {
            if (prices[i] < minPrice) {
                minPrice = prices[i];
            } else if (prices[i] - minPrice > maxProfit) {
                maxProfit = prices[i] - minPrice;
            }
        }
        return maxProfit;
    }


    public static void main(String[] args) {
        Stock stock = new Stock();
        int maxProfit = stock.maxProfit(new int[] {7,1,5,3,6,4});
        System.out.println(maxProfit);

        maxProfit = stock.maxProfit1(new int[] {7,1,5,3,6,4});
        System.out.println(maxProfit);
    }
}
