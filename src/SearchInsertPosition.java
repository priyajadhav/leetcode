/**
 * Solution to Search Insert Position Problem
 * Link : https://leetcode.com/problems/search-insert-position/
 */
public class SearchInsertPosition {
    public int searchInsert(int[] nums, int target) {
        if (nums == null) {
            return 0;
        }
        int size = nums.length;
        for (int i = 0; i < size; i++) {
            if (nums[i] >= target) {
                return i;
            }
        }
        return size;
    }

    // Using binary search approach since the array is sorted
    public int searchInsert1(int[] nums, int target) {
        if (nums == null) {
            return 0;
        }
        int i = 0;
        int j = nums.length;

        while (i < j) {
            int m = Math.abs(i + (j-i)/2);
            if (target == nums[m]) {
                return m;
            } else if (target < nums[m]) {
                j = m;
            } else {
                i = m + 1;
            }

        }
        return j;
    }

    public static void main(String[] args) {
        SearchInsertPosition removeDuplicates = new SearchInsertPosition();
        int[] nums = new int[]{1,2,3,5,6};
        int index = removeDuplicates.searchInsert(nums, 4);
        System.out.println(index);
        index = removeDuplicates.searchInsert1(nums, 4);
        System.out.println(index);
    }
}
