/**
 * Solution to Remove Duplicates From List problem
 * Link : https://leetcode.com/problems/remove-duplicates-from-sorted-list/
 */
public class RemoveDuplicatesFromList {
    public ListNode deleteDuplicates(ListNode head) {
        ListNode temp = head;
        if (temp == null || temp.next == null)
            return temp;
        while (temp != null) {
            if (temp.next != null && temp.next.val == temp.val) {
                temp.next = temp.next.next;
            } else {
                temp = temp.next;
            }
        }
        return head;
    }

    public static void main(String[] args) {
        ListNode l1 = new ListNode(1);
        l1.next = new ListNode(1);
        l1.next.next = new ListNode(1);
        l1.next.next.next = new ListNode(2);
        l1.next.next.next.next = new ListNode(3);
        l1.next.next.next.next.next = new ListNode(3);


        RemoveDuplicatesFromList rd = new RemoveDuplicatesFromList();
        ListNode result = rd.deleteDuplicates(l1);
        while(result != null) {
            System.out.println(result.val);
            result = result.next;
        }
    }
}
