import java.util.Arrays;

/**
 * Solution for Assign Cookies problem
 * Link : https://leetcode.com/problems/assign-cookies/
 */
public class AssignCookies {
    public int findContentChildren(int[] g, int[] s) {
        Arrays.sort(g);
        Arrays.sort(s);
        int i = 0, j=0;
        int count = 0;
        while (i < g.length && j < s.length) {
            if (s[j] >= g[i]) {
                i++; j++;
                count++;
            } else if (s[j] < g[i]) {
                j++;
            } else {
                i++;
            }

        }
        return count;
    }

    public static void main(String[] args) {
        AssignCookies fizzBuzz = new AssignCookies();
        System.out.println(fizzBuzz.findContentChildren(new int[]{10,9,8,7}, new int[]{5,6,7,8}));

    }
}
