/**
 * Link : https://leetcode.com/problems/jewels-and-stones/
 */
import java.util.HashSet;
import java.util.Set;

public class JewelsAndStones {
    public int numJewelsInStones(String J, String S) {
        if (J.isEmpty() || S.isEmpty()) return 0;
        Set<Character> set = new HashSet<>();
        char[] chars = J.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            set.add(chars[i]);
        }
        int count = 0;
        chars = S.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (set.contains(chars[i]))
                count++;
        }
        return count;
    }

    public static void main(String[] args)
    {
        JewelsAndStones demo = new JewelsAndStones();
        System.out.println(demo.numJewelsInStones("aA","aAAbbbb"));
    }
}
