import java.util.TreeSet;

/**
 * Solution to get third max number from array
 * Link : https://leetcode.com/problems/third-maximum-number/
 */
public class ThirdMax {
    public int thirdMax(int[] nums) {
        TreeSet<Integer> set = new TreeSet<>();
        for(int num : nums) {
            set.add(num);
            if(set.size() > 3) {
                set.remove(set.first());
            }
        }
        return set.size() == 3 ? set.first() : set.last();
    }

    public static void main(String[] args) {
        ThirdMax fizzBuzz = new ThirdMax();
        System.out.println(fizzBuzz.thirdMax(new int[]{10,9,8,7}));
    }
}
