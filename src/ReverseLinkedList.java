/**
 * Solution to Reverse Linked List
 * Link : https://leetcode.com/problems/reverse-linked-list/
 */
public class ReverseLinkedList {
    public ListNode reverseList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode result = new ListNode(head.val);
        head = head.next;
        while (head != null) {
            ListNode temp = result;
            result = new ListNode(head.val);
            result.next = temp;
            head = head.next;
        }
        return result;
    }


    public static void main(String[] args) {
        ListNode l1 = new ListNode(1);
        l1.next = new ListNode(2);
        l1.next.next = new ListNode(3);
        l1.next.next.next = new ListNode(4);
        l1.next.next.next.next = new ListNode(5);
        l1.next.next.next.next.next = new ListNode(6);

        ReverseLinkedList oddEvenLinkedList = new ReverseLinkedList();

        ListNode result = oddEvenLinkedList.reverseList(l1);
        while(result != null) {
            System.out.println(result.val);
            result = result.next;
        }
    }
}
