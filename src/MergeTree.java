/**
 * Solution to merge two binary trees
 * Link : https://leetcode.com/problems/merge-two-binary-trees/
 */
class MergeTree {
    public TreeNode mergeTrees(TreeNode t1, TreeNode t2) {
        if (t1 == null && t2 == null)
            return t1;
        if (t1 != null && t2 != null) {
            t1.val = t1.val + t2.val;
        } else if (t1 == null) {
            t1 = new TreeNode(t2.val);
        }
        t1.left = mergeTrees(t1 == null ? t1 : t1.left, t2 == null ? t2 : t2.left);
        t1.right = mergeTrees(t1 == null ? t1 : t1.right, t2 == null ? t2 : t2.right);
        return t1;
    }

    public static void main(String[] args) {
        TreeNode t1 = new TreeNode(1);
        t1.left = new TreeNode(3);
        t1.right = new TreeNode(2);
        t1.left.left = new TreeNode(5);

        TreeNode t2 = new TreeNode(2);
        t2.left = new TreeNode(1);
        t2.right = new TreeNode(3);
        t2.left.right = new TreeNode(4);
        t2.right.right = new TreeNode(7);

        TreeNode result = new MergeTree().mergeTrees(t1, t2);

        System.out.println(result);
    }
}
