import java.util.Date;

/**
 * Solution to problem of Length of last word
 * Link : https://leetcode.com/problems/length-of-last-word/
 */
public class LastWordLength {
    public int lengthOfLastWord(String s) {
        if (s == null || s.equals(" "))
            return 0;
        s = s.trim();
        int index = s.lastIndexOf(" ");
        if (index == -1) return s.length();
        return s.length() - 1 - index ;
    }

    public int lengthOfLastWord1(String s) {
        System.out.println(new Date());
        if (s.equals("")) return 0;
        char[] sArry= s.toCharArray();

        int length=0;
        int i;
        for (i=sArry.length-1;i>=0;i--){
            if (sArry[i]!=' ') break;
        }

        for (int j=i;j>=0;j--){
            if (sArry[j]==' ') break;
            length++;
        }
        return length;
    }

    public static void main(String[] args) {
        LastWordLength lastWordLength = new LastWordLength();
        System.out.println(lastWordLength.lengthOfLastWord("Hello World"));
        System.out.println(lastWordLength.lengthOfLastWord1("Hello World"));
    }
}
