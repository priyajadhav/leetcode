/**
 * Solution to check Same Tree
 * Link : https://leetcode.com/problems/same-tree/
 */
class SameTree {
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null && q == null)
            return true;
        if (p != null && q != null) {
            if (p.val != q.val)
                return false;
            return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
        }
        return false;
    }

    public static void main(String[] args) {
        TreeNode p = new TreeNode(1);
        p.left = new TreeNode(2);
        p.right = new TreeNode(3);
        p.left.left = new TreeNode(4);

        TreeNode q = new TreeNode(1);
        q.left = new TreeNode(2);
        q.right = null;

        System.out.println(new SameTree().isSameTree(p,q));
    }
}
