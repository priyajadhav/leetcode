import java.util.ArrayList;
import java.util.List;

/**
 * Solution for Fizz Buzz problem
 * Link : https://leetcode.com/problems/fizz-buzz/
 */
public class FizzBuzz {
    public List<String> fizzBuzz(int n) {
        List<String> list = new ArrayList<>();
        for (int i=1; i<=n; i++) {
            String str = null;
            if (i % 3 == 0)
                str = new String("Fizz");
            if (i % 5 == 0)
                str = str == null ? new String("Buzz") : str.concat("Buzz");
            if (str == null)
                str = String.valueOf(i);
            list.add(str);
        }
        return list;
    }

    public static void main(String[] args) {
        FizzBuzz fizzBuzz = new FizzBuzz();
        System.out.println(fizzBuzz.fizzBuzz(15));
    }
}
