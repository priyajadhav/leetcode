import java.util.HashMap;
import java.util.Map;

/**
 * Solution to convert Roman to Integer
 * Link : https://leetcode.com/problems/roman-to-integer/
 */
public class RomanToInt {
    public int romanToInt(String s) {
        Map<Character, Integer> map = new HashMap<>();
        map.put('I', 1);
        map.put('V', 5);
        map.put('X', 10);
        map.put('L', 50);
        map.put('C', 100);
        map.put('D', 500);
        map.put('M', 1000);

        int prevNumber = 0, result = 0;
        char[] chars = s.toCharArray();
        for(int i = 0; i < chars.length; i++) {
            int num = map.get(chars[i]);
            result += num;
            if ((num == 5 || num == 10) && prevNumber == 1)
                result = result - 2 * prevNumber;
            else if ((num == 50 || num == 100) && prevNumber == 10)
                result = result - 2 * prevNumber;
            else if ((num == 500 || num == 1000) && prevNumber == 100)
                result = result - 2 * prevNumber;
            prevNumber = num;
        }
        return result;
    }

    public static void main(String[] args) {
        RomanToInt romanToInt = new RomanToInt();
        System.out.println(romanToInt.romanToInt("LVIII"));
    }
}
